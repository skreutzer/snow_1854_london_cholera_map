/* Copyright (C) 2023 Gregory Nick Larkin, Stephan Kreutzer
 *
 * This file is part of Interactive Data Visualization of Dr. John Snow's
 * Map of the 1854 London Cholera Epidemic.
 *
 * The Interactive Data Visualization is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * The Interactive Data Visualization is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with the Interactive Data Visualization. If not, see <http://www.gnu.org/licenses/>.
 */


"use strict";


let mapPosScaleFactor = 25;

let svgMap = null;

function init(mapTargetId,
              mapViewportStartOffsetX,
              mapViewportStartOffsetY,
              mapViewportStartWidth,
              mapViewportStartHeight,
              timelineTargetId,
              genderDistributionTargetId,
              ageGroupTargetId)
{
    loadDeaths()
    .then(() =>
    {
        drawMap(mapTargetId,
                500,
                500,
                mapViewportStartOffsetX,
                mapViewportStartOffsetY,
                mapViewportStartWidth,
                mapViewportStartHeight);
        updateTimeline(timelineTargetId,
                       1000,
                       200);
        updateGenderDistribution(genderDistributionTargetId,
                                 250,
                                 250);
        // This could run in parallel (after death
        // data has been loaded, as ensured above).
        drawAgeGroups(ageGroupTargetId,
                      1000,
                      200);
    });
}

let mapWidth = -1;
let mapHeight = -1;

let mapMouseIsDown = false;
let mapMouseLastPosition = null;
let mapSkipClick = false;

// The map itself as background is static.
function drawMap(mapTargetId,
                 mapWidthSrc,
                 mapHeightSrc,
                 mapViewportStartOffsetX,
                 mapViewportStartOffsetY,
                 mapViewportStartWidth,
                 mapViewportStartHeight)
{
    mapWidth = mapWidthSrc;
    mapHeight = mapHeightSrc;

    svgMap = d3.select("#" + mapTargetId)
        .append("svg")
        .attr("id", "map")
        .attr("width", mapWidthSrc)
        .attr("height", mapHeightSrc)
        .attr("viewBox", "" + mapViewportStartOffsetX + " " + mapViewportStartOffsetY + " " + mapViewportStartWidth + " " + mapViewportStartHeight)
        .style("background-color", "white")
        .on("mousedown", (e) =>
        {
            mapMouseIsDown = true;
        })
        .on("mouseup", (e) =>
        {
            if (mapMouseLastPosition != null)
            {
                // There has been some movement,
                // so this is the end event for
                // it, let's not trigger another
                // click for it.

                mapSkipClick = true;
            }

            mapMouseIsDown = false;
            mapMouseLastPosition = null;
        })
        .on("mousemove", (e) =>
        {
            if (mapMouseIsDown == true)
            {
               mapMoveViewport(e);
            }
        })
        .on("wheel", (e) => { mapScaleViewport(e); })
        .on("click", (e) => { mapClick(e); });

    drawMapStreets();
}

function mapMoveViewport(event)
{
    if (mapMouseLastPosition == null)
    {
        mapMouseLastPosition = { x: event.clientX, y: event.clientY };
        return 0;
    }

    let diffX = mapMouseLastPosition.x - event.clientX;
    let diffY = mapMouseLastPosition.y - event.clientY;

    mapMouseLastPosition = null;

    let svgMapElement = document.getElementById("map");
    let args = svgMapElement.getAttribute("viewBox").split(" ");

    args[0] = parseInt(args[0].trim()) + diffX;
    args[1] = parseInt(args[1].trim()) + diffY;
    args[2] = parseInt(args[2].trim());
    args[3] = parseInt(args[3].trim());

    svgMapElement.setAttribute("viewBox", "" +
                                          args[0] + " " +
                                          args[1] + " " +
                                          args[2] + " " +
                                          args[3]);

    mapMouseLastPosition = { x: event.clientX, y: event.clientY };


    if (true)
    {
        // Grid filter markers fixed to the viewport and moving with it.

        let update = false;

        {
            let rangeCount = 0;

            for (let i = 0, max = currentMapRange[0].length; i < max; i++)
            {
                if (currentMapRange[0][i] != null)
                {
                    let transformation = currentMapRange[0][i].element.getAttribute("transform");
                    let args = transformation.slice(transformation.indexOf("(") + "(".length, transformation.indexOf(")"));
                    args = args.split(",");
                    args[0] = parseInt(args[0].trim());
                    args[1] = parseInt(args[1].trim());

                    currentMapRange[0][i].element.setAttribute("transform", "translate(" + (args[0] + diffX) + "," + (args[1] + diffY) + ")");
                    currentMapRange[0][i].x += diffX;

                    rangeCount += 1;
                }
            }

            if (rangeCount == 2)
            {
                update = true;
            }
        }

        {
            let rangeCount = 0;

            // TODO: All of these array access indexes as used here on
            // currentMapRange should be replaced by a properly named constant.
            for (let i = 0, max = currentMapRange[1].length; i < max; i++)
            {
                if (currentMapRange[1][i] != null)
                {
                    let transformation = currentMapRange[1][i].element.getAttribute("transform");
                    let args = transformation.slice(transformation.indexOf("(") + "(".length, transformation.indexOf(")"));
                    args = args.split(",");
                    args[0] = parseInt(args[0].trim());
                    args[1] = parseInt(args[1].trim());

                    currentMapRange[1][i].element.setAttribute("transform", "translate(" + (args[0] + diffX) + "," + (args[1] + diffY) + ")");
                    currentMapRange[1][i].y += diffY;

                    rangeCount += 1;
                }
            }

            if (rangeCount == 2)
            {
                update = true;
            }
        }

        if (update == true)
        {
            // OK, this might be somewhat costly, for removing + recreating every
            // death on the map following the timeline or its filter, on each
            // move event, instead of just updating the last stage of deaths
            // that were rendered/visible previously.
            updateMapDeaths();
        }
    }
    else
    {
        // Not doing the above: grid filter markers fixed to the map
        // (its absolute world/data coordinates).
    }
}

// First dimension: x/y axis.
let currentMapRange = new Array(2);
// Second dimension: start/end marker.
currentMapRange[0] = new Array(2);
currentMapRange[1] = new Array(2);

function mapClick(event)
{
    if (mapMouseIsDown == true)
    {
        event.preventDefault();
        return 1;
    }

    if (mapSkipClick == true)
    {
        event.preventDefault();
        mapSkipClick = false;
        return 1;
    }

    let target = document.getElementById("map");

    let absoluteMapClickPosition = null;

    {
        let transformPoint = target.createSVGPoint();
        transformPoint.x = event.clientX;
        transformPoint.y = event.clientY;

        absoluteMapClickPosition = transformPoint.matrixTransform(target.getScreenCTM().inverse());
    }

    if (event.layerX > event.layerY)
    {
        let rangeCount = 0;

        for (let i = 0, max = currentMapRange[0].length; i < max; i++)
        {
            if (currentMapRange[0][i] != null)
            {
                rangeCount += 1;
            }
        }

        let update = false;

        if (rangeCount == 2)
        {
            update = true;

            if (currentMapRange[0][0].x < absoluteMapClickPosition.x &&
                absoluteMapClickPosition.x < currentMapRange[0][1].x)
            {
                // This could be improved by tracking from which direction the mouse hover comes
                // from, and a click into the inside of the previous range selection moves the
                // grid limiter inside towards the other, from where the mouse is coming from.

                let distanceLhs = absoluteMapClickPosition.x - currentMapRange[0][0].x;
                let distanceRhs = currentMapRange[0][1].x - absoluteMapClickPosition.x;

                if (distanceLhs < distanceRhs)
                {
                    currentMapRange[0][0].element.parentNode.removeChild(currentMapRange[0][0].element);
                    currentMapRange[0][0] = null;

                    rangeCount -= 1;
                }
                else
                {
                    currentMapRange[0][1].element.parentNode.removeChild(currentMapRange[0][1].element);
                    currentMapRange[0][1] = null;

                    rangeCount -= 1;
                }
            }
            else if (absoluteMapClickPosition.x < currentMapRange[0][1].x)
            {
                currentMapRange[0][0].element.parentNode.removeChild(currentMapRange[0][0].element);
                currentMapRange[0][0] = null;

                rangeCount -= 1;
            }
            else if (absoluteMapClickPosition.x > currentMapRange[0][1].x)
            {
                currentMapRange[0][1].element.parentNode.removeChild(currentMapRange[0][1].element);
                currentMapRange[0][1] = null;

                rangeCount -= 1;
            }
            else
            {
                // This should never happen - one needs to be removed here,
                // for the new one to be added.
                throw new Exception("Unexpected condition.");
            }
        }


        let svgMapElement = document.getElementById("map");
        let args = svgMapElement.getAttribute("viewBox").split(" ");

        args[0] = parseInt(args[0].trim());
        args[1] = parseInt(args[1].trim());
        args[2] = parseInt(args[2].trim());
        args[3] = parseInt(args[3].trim());

        let scaleFactor = (args[2] / mapWidth);
        let markerWidth = 25;
        let markerWidthScaled = markerWidth * scaleFactor;

        let markerGroup = document.createElementNS("http://www.w3.org/2000/svg", "g");
        // TODO: class
        markerGroup.setAttribute("transform", "translate(" + (args[0] + (((event.clientX - target.getBoundingClientRect().left) * scaleFactor) - (markerWidthScaled / 2))) + "," + args[1] + ")");
        //markerGroup.addEventListener("click", () => {  });

        let markerLine = document.createElementNS("http://www.w3.org/2000/svg", "path");
        // If there's two markers set, the marker triangle could be made to point inwards the selected filtering timespan selection.
        markerLine.setAttribute("d", "M " + (markerWidthScaled / 2) + " 0 " +
                                     "L " + (markerWidthScaled / 2) + " " + (args[3]));
        markerLine.setAttribute("stroke", "#FFA500");
        markerLine.setAttribute("stroke-width", (2 * scaleFactor));
        markerGroup.appendChild(markerLine);

        let marker = document.createElementNS("http://www.w3.org/2000/svg", "polygon");
        marker.setAttribute("points", "0,0 " +
                                      markerWidthScaled + ",0 " +
                                      (markerWidthScaled / 2) + "," + (markerWidthScaled / 1.5));
        marker.setAttribute("fill", "#FFA500");
        markerGroup.appendChild(marker);

        target.appendChild(markerGroup);


        let newEntry = { x: absoluteMapClickPosition.x,
                         element: markerGroup };

        if (rangeCount == 0)
        {
            currentMapRange[0][0] = newEntry;
            return 0;
        }
        else if (rangeCount == 1)
        {
            // Put the new element into the empty slot, swap/sort later.

            if (currentMapRange[0][0] == null)
            {
                currentMapRange[0][0] = newEntry;
            }
            else
            {
                currentMapRange[0][1] = newEntry;
            }

            if (currentMapRange[0][0].x > currentMapRange[0][1].x)
            {
                // Swap.
                let temp = currentMapRange[0][0];
                currentMapRange[0][0] = currentMapRange[0][1];
                currentMapRange[0][1] = temp;
            }

            update = true;
        }

        if (update == true)
        {
            updateMapDeaths();
        }
    }
    else
    {
        let rangeCount = 0;

        for (let i = 0, max = currentMapRange[1].length; i < max; i++)
        {
            if (currentMapRange[1][i] != null)
            {
                rangeCount += 1;
            }
        }

        let update = false;

        if (rangeCount == 2)
        {
            update = true;

            if (currentMapRange[1][0].y < absoluteMapClickPosition.y &&
                absoluteMapClickPosition.y < currentMapRange[1][1].y)
            {
                // This could be improved by tracking from which direction the mouse hover comes
                // from, and a click into the inside of the previous range selection moves the
                // grid limiter inside towards the other, from where the mouse is coming from.

                let distanceLhs = absoluteMapClickPosition.y - currentMapRange[1][0].y;
                let distanceRhs = currentMapRange[1][1].y - absoluteMapClickPosition.y;

                if (distanceLhs < distanceRhs)
                {
                    currentMapRange[1][0].element.parentNode.removeChild(currentMapRange[1][0].element);
                    currentMapRange[1][0] = null;

                    rangeCount -= 1;
                }
                else
                {
                    currentMapRange[1][1].element.parentNode.removeChild(currentMapRange[1][1].element);
                    currentMapRange[1][1] = null;

                    rangeCount -= 1;
                }
            }
            else if (absoluteMapClickPosition.y < currentMapRange[1][1].y)
            {
                currentMapRange[1][0].element.parentNode.removeChild(currentMapRange[1][0].element);
                currentMapRange[1][0] = null;

                rangeCount -= 1;
            }
            else if (absoluteMapClickPosition.y > currentMapRange[1][1].y)
            {
                currentMapRange[1][1].element.parentNode.removeChild(currentMapRange[1][1].element);
                currentMapRange[1][1] = null;

                rangeCount -= 1;
            }
            else
            {
                // This should never happen - one needs to be removed here,
                // for the new one to be added.
                throw new Exception("Unexpected condition.");
            }
        }


        let svgMapElement = document.getElementById("map");
        let args = svgMapElement.getAttribute("viewBox").split(" ");

        args[0] = parseInt(args[0].trim());
        args[1] = parseInt(args[1].trim());
        args[2] = parseInt(args[2].trim());
        args[3] = parseInt(args[3].trim());

        let scaleFactor = (args[3] / mapHeight);
        let markerHeight = 25;
        let markerHeightScaled = markerHeight * scaleFactor;

        let target = document.getElementById("map");

        let markerGroup = document.createElementNS("http://www.w3.org/2000/svg", "g");
        // TODO: class
        markerGroup.setAttribute("transform", "translate(" + args[0] + "," + (args[1] + (((event.clientY - target.getBoundingClientRect().top) * scaleFactor) - (markerHeightScaled / 2))) + ")");
        //markerGroup.addEventListener("click", () => {  });

        let markerLine = document.createElementNS("http://www.w3.org/2000/svg", "path");
        // If there's two markers set, the marker triangle could be made to point inwards the selected filtering timespan selection.
        markerLine.setAttribute("d", "M 0 " + (markerHeightScaled / 2) + " " +
                                     "L " + args[2] + " " + (markerHeightScaled / 2));
        markerLine.setAttribute("stroke", "#FFA500");
        markerLine.setAttribute("stroke-width", (2 * scaleFactor));
        markerGroup.appendChild(markerLine);

        let marker = document.createElementNS("http://www.w3.org/2000/svg", "polygon");
        marker.setAttribute("points", "0,0 " +
                                      "0," + markerHeightScaled + " " +
                                      (markerHeightScaled / 1.5) + "," + (markerHeightScaled / 2));
        marker.setAttribute("fill", "#FFA500");
        markerGroup.appendChild(marker);

        target.appendChild(markerGroup);


        let newEntry = { y: absoluteMapClickPosition.y,
                         element: markerGroup };

        if (rangeCount == 0)
        {
            currentMapRange[1][0] = newEntry;
            return 0;
        }
        else if (rangeCount == 1)
        {
            // Put the new element into the empty slot, swap/sort later.

            if (currentMapRange[1][0] == null)
            {
                currentMapRange[1][0] = newEntry;
            }
            else
            {
                currentMapRange[1][1] = newEntry;
            }

            if (currentMapRange[1][0].y > currentMapRange[1][1].y)
            {
                // Swap.
                let temp = currentMapRange[1][0];
                currentMapRange[1][0] = currentMapRange[1][1];
                currentMapRange[1][1] = temp;
            }

            update = true;
        }

        if (update == true)
        {
            updateMapDeaths();
        }
    }
}

function mapScaleViewport(event)
{
    event.preventDefault();

    let svgMapElement = document.getElementById("map");
    let args = svgMapElement.getAttribute("viewBox").split(" ");

    args[0] = parseInt(args[0].trim());
    args[1] = parseInt(args[1].trim());
    args[2] = parseInt(args[2].trim());
    args[3] = parseInt(args[3].trim());


    // TODO: Implement this!
    // Could also use an exponential zooming factor
    // instead of fixed coordinate offsets. Then there
    // would also never be a 0.0 reached for width/height,
    // and could zoom in indefinitely. With the absolute
    // offsets used here, zoom + movement remain relative to
    // the distances at the initial 100% scale.
    /*
    let mapZoomFactorOffset = 0.05;
    let mapZoomFactor = 1.0;

    if (event.deltaY > 0)
    {
        if (mapZoomFactor - 0.05 > 0.0)
        {
            mapZoomFactor += mapZoomFactorOffset;
        }
    }
    else if (event.deltaY < 0)
    {
        mapZoomFactor -= mapZoomFactorOffset;
    }

    let mapViewportXOffset = (((mapWidth * mapZoomFactor) - mapWidth) / 2);
    let mapViewportYOffset = ((((mapHeight * mapZoomFactor) - mapHeight) / 2));

    let svgMapElement = document.getElementById("map");
    let args = svgMapElement.getAttribute("viewBox").split(" ");

    args[0] = parseInt(args[0].trim()) - mapViewportXOffset;
    args[1] = parseInt(args[1].trim()) - mapViewportYOffset;
    args[2] = parseInt(args[2].trim()) * mapZoomFactor;
    args[3] = parseInt(args[3].trim()) * mapZoomFactor;

    svgMapElement.setAttribute("viewBox", "" +
                                          args[0] + " " +
                                          args[1] + " " +
                                          args[2] + " " +
                                          args[3]);
    */


    let zoomOffset = 0;

    // So the coordinates from the source get multiplied by
    // mapPosScaleFactor to be the "default" zoom scale of 100%.
    // So we're using that factor as zoom "steps"/offset here.
    if (event.deltaY > 0)
    {
        zoomOffset += mapPosScaleFactor;
    }
    else if (event.deltaY < 0)
    {
        if (args[2] - (mapPosScaleFactor * 2) > 0.0 &&
            args[3] - (mapPosScaleFactor * 2) > 0.0)
        {
            zoomOffset -= mapPosScaleFactor;
        }
        else
        {
            return 1;
        }
    }

    // Zooming the viewport relative to the center of the SVG.
    // Could also be centered at the current mouse location, using
    // the mouse coordinates as provided in event.
    args[0] = args[0] + (zoomOffset * -1);
    args[1] = args[1] + (zoomOffset * -1);
    args[2] = args[2] + (zoomOffset * 2);
    args[3] = args[3] + (zoomOffset * 2);

    svgMapElement.setAttribute("viewBox", "" +
                                          args[0] + " " +
                                          args[1] + " " +
                                          args[2] + " " +
                                          args[3]);

    // TODO: Zoom map grid filter markers?
}

function drawMapStreets()
{
    Promise.all([d3.json("./data/streets.json"),
                 d3.json("./data/streets_names.json"),
                 d3.json("./data/points_of_interest.json")])
    .then(function(data)
    {
        {
            let streets = data[0];

            let line = d3.line()
                .x(function(d, i)
                {
                    return d.x * mapPosScaleFactor;
                })
                .y(function(d, i)
                {
                    return mapHeight - (d.y * mapPosScaleFactor);
                });

            // Should put these into a <g/>?
            svgMap.selectAll("path.street")
                .data(streets)
                .enter()
                .append("path")
                .attr("class", "street")
                .attr("d", line)
                .attr("fill", "none")
                .attr("stroke", "grey")
                .attr("stroke-width", 2);
        }

        {
            let streetLabels = new Array();

            for (const streetName of data[1])
            {
                for (const label of streetName)
                {
                    streetLabels.push({ x: parseFloat(label.x) * mapPosScaleFactor,
                                        y: mapHeight - (parseFloat(label.y) * mapPosScaleFactor),
                                        label: label.label,
                                        angle: label.angle,
                                        fontSize: label["font-size"] });
                }
            }

            svgMap.selectAll("text.street-name")
                .data(streetLabels)
                .enter()
                .append("text")
                .attr("class", "street-name")
                .text((d) => d.label)
                .attr("font-size", (d) => d.fontSize)
                .attr("transform", (d) => "translate(" + d.x + "," + d.y + ") rotate(" + d.angle + ")");
        }

        {
            let pois = new Array();

            for (const poi of data[2])
            {
                let polygon = "";

                for (let i = 0, max = poi.points.length; i < max; i++)
                {
                    if (i > 0)
                    {
                        polygon += " ";
                    }

                    polygon += (parseFloat(poi.points[i].x) * mapPosScaleFactor) + "," +
                               (mapHeight - ((parseFloat(poi.points[i].y) * mapPosScaleFactor)));
                }

                pois.push( { name: poi.name,
                             color: poi.color,
                             polygon: polygon });
            }

            svgMap.selectAll("polygon.poi")
                .data(pois)
                .enter()
                .append("polygon")
                .attr("points", (d) => d.polygon)
                .attr("fill", (d) => d.color);
        }

        // Not then()-chaining, ensures the
        // synchronous/sequential execution.
        drawMapPumps();
    });
}

function drawMapPumps()
{
    d3.csv('./data/pumps.csv')
        .then(function(pumps)
        {
			pumps.forEach(function (d)
			{
				d.x = parseFloat(d.x) * mapPosScaleFactor;
				d.y = mapHeight - (parseFloat(d.y)*mapPosScaleFactor);
			});

            let pumpSquareSize = 8;
            let pumpSquarePosOffset = pumpSquareSize / 2;

			svgMap.selectAll("rect.pump")
				.data(pumps)
				.enter()
				.append("rect")
                .attr("class", "pump")
				.attr("x", function(d) { return d.x - pumpSquarePosOffset; } ) // accessor sets x to the data point's x
				.attr("y", function(d) { return d.y - pumpSquarePosOffset; } ) // accessor sets y to 100 - the data point's y
				.attr("width", pumpSquareSize)
				.attr("height", pumpSquareSize);
        })
        // Same as .then(function() { updateMapDeaths(); })
        .then(() => { updateMapDeaths(); });
}

let deathsPerDay = null;

function loadDeaths()
{
    // Same here, returning a Promise for then()-chaining not needed
    // any more, not going to chain/run multiple separate copies of
    // it any more.
    return new Promise(async function(resolve, reject)
    {
        // OK, this guard is not needed any more, as this function
        // is not called multiple times any more. And can't, because
        // that would return multiple Promises, each executing separately.
        // Instead, it's now made sure that loadDeaths() is completed
        // once and first, and then several other functions running in
        // parallel for rendering may use the data it loaded first.
        if (deathsPerDay != null)
        {
            resolve();
            return 1;
        }

        deathsPerDay = new Map();

        Promise.all([d3.csv("./data/deathdays.csv"),
                     d3.csv("./data/deaths_age_sex.csv")])
        .then(function(data)
        {
            {
                let death_days = data[0];

                let timeFormatParseFunction = d3.timeParse("%d-%b-%Y");

                for (let i = 0, max = death_days.length; i < max; i++)
                {
                    let deathDay = { day: i + 1,
                                     // TODO: This may not work entirely right, for giving a timezone
                                     // offset of +0053, which is suspiciously close to the 54 of
                                     // the 1854 year...
                                     date: timeFormatParseFunction(death_days[i].date + "-1854"),
                                     deathCount: parseInt(death_days[i].deaths),
                                     deaths: new Array() };

                    deathsPerDay.set(i + 1, deathDay);
                }
            }

            {
                let deaths_age_sex = data[1];

                let deathDay = 1;
                let deathsOnDayCount = 0;

                for (let i = 0, max = deaths_age_sex.length; i < max; i++)
                {
                    deathsOnDayCount++;

                    if (deathsOnDayCount > deathsPerDay.get(deathDay).deathCount)
                    {
                        do
                        {
                            deathDay++;

                        } while (deathsPerDay.get(deathDay).deathCount <= 0);

                        deathsOnDayCount = 1;
                    }

                    deathsPerDay.get(deathDay).deaths.push({ x: parseFloat(deaths_age_sex[i].x),
                                                             y: parseFloat(deaths_age_sex[i].y),
                                                             gender: parseInt(deaths_age_sex[i].gender),
                                                             age: parseInt(deaths_age_sex[i].age) });
                }
            }

            for (const [deathDay, values] of deathsPerDay)
            {
                let dayDeathsPerGender = new Map();

                for (let i = 0, max = values.deaths.length; i < max; i++)
                {
                    let gender = values.deaths[i].gender;

                    if (dayDeathsPerGender.has(gender) != true)
                    {
                        dayDeathsPerGender.set(gender, 1);
                    }
                    else
                    {
                        dayDeathsPerGender.set(gender, dayDeathsPerGender.get(gender) + 1);
                    }
                }

                deathsPerDay.get(deathDay).deathCountPerGender = dayDeathsPerGender;
            }

            // This one here is very important! Makes sure the
            // promises then() callback chain executes in proper
            // synchronous order, but only from this line on,
            // once this local then() ran + completed. Don't put
            // it outside, into the Promise function's scope,
            // because that will run and complete, and not be
            // dependent on above D3's loader completing.
            // The parameter name "resolve", which contains
            // the callback function, is available here locally
            // via outside capture.
            resolve();
        });
    });
}

let genderRedColors = ["#fee5d9","#fcbba1","#fc9272","#fb6a4a","#de2d26","#a50f15"];
let genderBlueColors = ["#eff3ff","#c6dbef","#9ecae1","#6baed6","#3182bd","#08519c"];

function updateMapDeaths()
{
    {
        let deathCircles = document.querySelectorAll("circle.death");

        for (let i = 0, max = deathCircles.length; i < max; i++)
        {
            deathCircles.item(i).parentNode.removeChild(deathCircles.item(i));
        }
    }

    if (currentDeathDaySelection < 1)
    {
        return 1;
    }

    let deathDaysRangeFilterCount = 0;

    for (let i = 0, max = currentDeathDayRange.length; i < max; i++)
    {
        if (currentDeathDayRange[i] != null)
        {
            deathDaysRangeFilterCount += 1;
        }
    }

    let mapRangeFilterCountX = 0;

    for (let i = 0, max = currentMapRange[0].length; i < max; i++)
    {
        if (currentMapRange[0][i] != null)
        {
            mapRangeFilterCountX += 1;
        }
    }

    let mapRangeFilterCountY = 0;

    for (let i = 0, max = currentMapRange[1].length; i < max; i++)
    {
        if (currentMapRange[1][i] != null)
        {
            mapRangeFilterCountY += 1;
        }
    }

    // We also recreate the additive/cumulative deaths per day till
    // the requested day, instead of precomputing/caching.
    let data = new Array();

    for (const [deathDay, values] of deathsPerDay)
    {
        // This acts as a filter.
        if (deathDay > currentDeathDaySelection)
        {
            break;
        }

        // In addition, apply the timeline range selection filter
        // limiting at which day the rendering starts and at which
        // day it stops.
        if (deathDaysRangeFilterCount == 2)
        {
            if (deathDay < currentDeathDayRange[0].day)
            {
                continue;
            }

            if (deathDay > currentDeathDayRange[1].day)
            {
                break;
            }
        }

        for (let i = 0, max = values.deaths.length; i < max; i++)
        {
            if (currentGenderSelection >= 0)
            {
                // This acts as a filter too.
                if (currentGenderSelection != values.deaths[i].gender)
                {
                    continue;
                }
            }

            // OK, here we re-calculate x/y for every update all over again,
            // instead of saving/caching it, but on the other hand, it really
            // belongs to the GUI/vizualization and not the source/domain data.
            let x = values.deaths[i].x * mapPosScaleFactor;
            let y = mapHeight - (values.deaths[i].y * mapPosScaleFactor);

            // And another filter.
            if (mapRangeFilterCountX == 2)
            {
                if (x < currentMapRange[0][0].x ||
                    x > currentMapRange[0][1].x)
                {
                    continue;
                }
            }

            // And another filter.
            if (mapRangeFilterCountY == 2)
            {
                if (y < currentMapRange[1][0].y ||
                    y > currentMapRange[1][1].y)
                {
                    continue;
                }
            }

            data.push({ x: x,
                        y: y,
                        gender: values.deaths[i].gender,
                        age: values.deaths[i].age });
        }

        svgMap.selectAll("circle.death")
            // D3 has the problem that it needs the complete data to be provided.
            .data(data)
            .enter()
            .append("circle")
            .attr("class", "death")
            .attr("r", 4)//your radius value
            .attr("cx", function (d) { return d.x ; })
            .attr("cy", function (d) { return d.y ; })
            .style("fill", function (d) 
            {
                if(d.gender == 1)
                {
                    return genderRedColors[d.age];

                }
                else if (d.gender == 0)
                {
                    return genderBlueColors[d.age];
                }
                else
                {
                    return "#FFFFFF";
                }
            })
            .style("stroke", "gray");
    }
}

let timelineWidth = -1;
let timelineHeight = -1;

let deathDaysCount = -1;

let svgTimeline = null;
let currentDeathDaySelection = -1;

let currentDeathDayRange = new Array(2);

function updateTimeline(timelineTargetId,
                        timelineWidthSrc,
                        timelineHeightSrc)
{
    timelineWidth = timelineWidthSrc;
    timelineHeight = timelineHeightSrc;

    let isUpdate = false;

    {
        let target = document.getElementById("timeline");

        if (target != null)
        {
            isUpdate = true;
            target.parentNode.removeChild(target);
        }
    }

    svgTimeline = d3.select("#" + timelineTargetId)
		.append("svg")
        .attr("id", "timeline")
		.attr("width", timelineWidth)
		.attr("height", timelineHeight)
		.style("background-color", "white");

    let data = new Array();
    let dayDeathsMax = 0;

    for (const [deathDay, values] of deathsPerDay)
    {
        let deathCount = 0;

        if (currentGenderSelection >= 0)
        {
            if (values.deathCountPerGender.has(currentGenderSelection) == true)
            {
                deathCount = values.deathCountPerGender.get(currentGenderSelection);
            }
            else
            {
                deathCount = 0;
            }
        }
        else
        {
            deathCount = values.deathCount;
        }

        data.push({ day: deathDay,
                    deaths: deathCount });

        if (values.deathCount > dayDeathsMax)
        {
            dayDeathsMax = values.deathCount;
        }
    }

    deathDaysCount = data.length;
    let padding = 1;
    let dayWidth = (timelineWidth - (deathDaysCount * padding)) / deathDaysCount;

    let dayDeathHeightFactor = timelineHeight / dayDeathsMax;

    svgTimeline.selectAll("rect.death-day")
        .data(data)
        .enter()
        .append("rect")
        .attr("class", "death-day")
        .attr("x", (d, i) => { return (i * dayWidth) + (i * padding); })
        .attr("y", (d, i) => { return timelineHeight - (d.deaths * dayDeathHeightFactor); })
        .attr("width", dayWidth)
        .attr("height", (d, i) => { return d.deaths * dayDeathHeightFactor; })
        .attr("fill", "blue")
        .on("mouseenter", (e, d) => updateDeathDay(e, d))
        .on("click", (e, d) => clickDeathDay(e, d));

    svgTimeline.selectAll("rect.death-day-remainder")
        .data(data)
        .enter()
        .append("rect")
        .attr("class", "death-day-remainder")
        .attr("x", (d, i) => { return (i * dayWidth) + (i * padding); })
        .attr("y", "0")
        .attr("width", dayWidth)
        .attr("height", (d, i) => { return timelineHeight - (d.deaths * dayDeathHeightFactor); })
        .attr("fill", "#D3D3D3")
        .on("mouseenter", (e, d) => updateDeathDay(e, d))
        .on("click", (e, d) => clickDeathDay(e, d));

    svgTimeline.selectAll("text.death-day-label-count")
        .data(data)
        .enter()
        .append("text")
        .attr("class", "death-day-label-count")
        .attr("fill", "#FFFFFF")
        .text((d) => d.deaths)
        // Well, OK, can place these at the bottom, or only display on hover, etc.
        .attr("transform", (d, i) => "translate(" + ((i * dayWidth) + (i * padding) + 5) + ",15) rotate(90)");

    // TODO: Maybe use this instead of custom text labels?
    /*
    let xScale = d3.scaleBand().range ([0, 1000]).padding(0.4);
	let yScale = d3.scaleLinear().range ([200, 0]);

	let y_scale = d3.scaleLinear()
		.domain([0, 150])
        .range([0,200]);
    */

    if (isUpdate == true)
    {
        // Copy, clone.
        let deathDayRangeCopy = currentDeathDayRange.slice();

        currentDeathDayRange[0] = null;
        currentDeathDayRange[1] = null;

        for (let i = 0, max = deathDayRangeCopy.length; i < max; i++)
        {
            if (deathDayRangeCopy[i] != null)
            {
                clickDeathDay(null, { day: deathDayRangeCopy[i].day });
            }
        }
    }
}

function updateDeathDay(event, data)
{
    currentDeathDaySelection = data.day;
    updateMapDeaths();
}

function clickDeathDay(event, data)
{
    let update = false;
    let rangeCount = 0;

    {
        let deselectionCount = 0;

        for (let i = 0, max = currentDeathDayRange.length; i < max; i++)
        {
            if (currentDeathDayRange[i] != null)
            {
                rangeCount += 1;

                if (currentDeathDayRange[i].day == data.day)
                {
                    // OK, it's a deselection.

                    currentDeathDayRange[i].element.parentNode.removeChild(currentDeathDayRange[i].element);

                    currentDeathDayRange[i] = null;

                    deselectionCount += 1;
                }
            }
        }

        if (deselectionCount > 0)
        {
            if (rangeCount >= 2)
            {
                {
                    // TODO: Not nice. Refactor eventually.

                    let pie = document.getElementById("pie");

                    if (pie != null)
                    {
                        updateGenderDistribution(pie.parentNode.getAttribute("id"),
                                                pieWidth,
                                                pieHeight);
                    }
                }

                updateMapDeaths();
            }

            return 1;
        }
    }

    if (rangeCount == 2)
    {
        update = true;

        if (currentDeathDayRange[0].day < data.day && data.day < currentDeathDayRange[1].day)
        {
            // This could be improved by tracking from which direction the mouse hover comes
            // from, and a click into the inside of the previous range selection moves the
            // range limiter inside towards the other, from where the mouse is coming from.

            let distanceLhs = data.day - currentDeathDayRange[0].day;
            let distanceRhs = currentDeathDayRange[1].day - data.day;

            if (distanceLhs < distanceRhs)
            {
                currentDeathDayRange[0].element.parentNode.removeChild(currentDeathDayRange[0].element);
                currentDeathDayRange[0] = null;

                rangeCount -= 1;
            }
            else
            {
                currentDeathDayRange[1].element.parentNode.removeChild(currentDeathDayRange[1].element);
                currentDeathDayRange[1] = null;

                rangeCount -= 1;
            }
        }
        else if (data.day < currentDeathDayRange[1].day)
        {
            currentDeathDayRange[0].element.parentNode.removeChild(currentDeathDayRange[0].element);
            currentDeathDayRange[0] = null;

            rangeCount -= 1;
        }
        else if (data.day > currentDeathDayRange[1].day)
        {
            currentDeathDayRange[1].element.parentNode.removeChild(currentDeathDayRange[1].element);
            currentDeathDayRange[1] = null;

            rangeCount -= 1;
        }
        else
        {
            // This should never happen - one needs to be removed here,
            // for the new one to be added.
            throw new Exception("Unexpected condition.");
        }
    }

    let padding = 1;
    let dayWidth = (timelineWidth - (deathDaysCount * padding)) / deathDaysCount;
    let markerWidth = dayWidth / 1.5;

    let target = document.getElementById("timeline");

    let markerGroup = document.createElementNS("http://www.w3.org/2000/svg", "g");
    // TODO: class
    // If there's two markers set, the end marker might be added on the right half/side of the bar.
    markerGroup.setAttribute("transform", "translate(" + (((data.day - 1) * dayWidth) + ((data.day - 1) * padding)) + ",0)");
    markerGroup.addEventListener("click", () => { clickDeathDay(null, { day: data.day }); });

    let markerLine = document.createElementNS("http://www.w3.org/2000/svg", "path");
    // If there's two markers set, the marker triangle could be made to point inwards the selected filtering timespan selection.
    markerLine.setAttribute("d", "M " + (markerWidth / 2) + " 0 L " + (markerWidth / 2) + " " + timelineHeight);
    markerLine.setAttribute("stroke", "#FFA500");
    markerLine.setAttribute("stroke-width", 2);
    markerGroup.appendChild(markerLine);

    let marker = document.createElementNS("http://www.w3.org/2000/svg", "polygon");
    marker.setAttribute("points", "0,0 " + markerWidth + ",0 " + (markerWidth / 2) + "," + (markerWidth / 1.5));
    marker.setAttribute("fill", "#FFA500");
    markerGroup.appendChild(marker);

    target.appendChild(markerGroup);


    let newEntry = { day: data.day,
                     element: markerGroup };

    if (rangeCount == 0)
    {
        currentDeathDayRange[0] = newEntry;
        rangeCount++;
    }
    else if (rangeCount == 1)
    {
        // Put the new element into the empty slot, swap/sort later.

        if (currentDeathDayRange[0] == null)
        {
            currentDeathDayRange[0] = newEntry;
        }
        else
        {
            currentDeathDayRange[1] = newEntry;
        }

        rangeCount++;
        update = true;

        if (currentDeathDayRange[0].day > currentDeathDayRange[1].day)
        {
            // Swap.
            let temp = currentDeathDayRange[0];
            currentDeathDayRange[0] = currentDeathDayRange[1];
            currentDeathDayRange[1] = temp;
        }
    }

    if (update == true)
    {
        {
            // TODO: Not nice. Refactor eventually.

            let pie = document.getElementById("pie");

            if (pie != null)
            {
                updateGenderDistribution(pie.parentNode.getAttribute("id"),
                                        pieWidth,
                                        pieHeight);
            }
        }

        updateMapDeaths();
    }
}

let pieWidth = -1;
let pieHeight = -1;

function updateGenderDistribution(genderDistributionTargetId,
                                  pieWidthSrc,
                                  pieHeightSrc)
{
    {
        let target = document.getElementById("pie");

        if (target != null)
        {
            target.parentNode.removeChild(target);
        }
    }

    pieWidth = pieWidthSrc;
    pieHeight = pieHeightSrc;


    let rangeCount = 0;

    for (let i = 0, max = currentDeathDayRange.length; i < max; i++)
    {
        if (currentDeathDayRange[i] != null)
        {
            rangeCount += 1;
        }
    }

    let data = new Map();

    let colors = new Array();
    colors[0] = genderBlueColors[Math.round(genderBlueColors.length / 2)];
    colors[1] = genderRedColors[Math.round(genderRedColors.length / 2)];

    for (const [deathDay, values] of deathsPerDay)
    {
        // Another filter.
        if (rangeCount == 2)
        {
            if (deathDay < currentDeathDayRange[0].day ||
                deathDay > currentDeathDayRange[1].day)
            {
                continue;
            }
        }

        for (let i = 0, max = values.deaths.length; i < max; i++)
        {
            let color = "#FFFFFF";

            if (typeof colors[values.deaths[i].gender] != "undefined")
            {
                color = colors[values.deaths[i].gender];
            }

            if (data.has(values.deaths[i].gender) != true)
            {
                data.set(values.deaths[i].gender, { count: 1,
                                                    color: color });
            }
            else
            {
                data.get(values.deaths[i].gender).count = data.get(values.deaths[i].gender).count + 1;
            }
        }
    }

    let radius = Math.min(pieWidth, pieHeight) / 2;

    let svg = d3.select("#" + genderDistributionTargetId)
        .append("svg")
        .attr("id", "pie")
        .attr("width", pieWidth)
        .attr("height", pieHeight)
        .append("g")
        .attr("class", "pieChartContainer")
        // transform/translate: move the circle's center to the SVG's center.
        .attr("transform", "translate(" + (pieWidth / 2) + "," + (pieHeight / 2) + ")");

    let pieValueSelector = d3.pie()
        .value((d) => { return d[1].count; });

    if (true)
    {
        let arcGenerator = d3.arc()
            .innerRadius(0)
            .outerRadius(radius);

        let arcs = svg.selectAll("g.pieArc")
            .data(pieValueSelector(data))
            .enter()
            .append("g")
            .attr("class", "pieArc");

        arcs.insert("path")
            .attr("class", "gender")
            .attr("d", arcGenerator)
            // Can also be written as expression for evaluation,
            // no need to have curly braces and semicolon for
            // multi-line executable code statements. But then
            // is limited to only expressions though.
            .attr("fill", (d) => d.data[1].color)
            .on("click", (e, d) => { genderDistributionClick(d.data[0]); });

        arcs.insert("text")
            .text((d) => d.data[1].count)
            .attr("transform", (d) => "translate(" + arcGenerator.centroid(d) + ")")
            .attr("fill", "#FFFFFF")
            .style("text-anchor", "middle");
    }
    else
    {
        svg.selectAll("path.gender")
            .data(pieValueSelector(data))
            .enter()
            .append("path")
            .attr("class", "gender")
            .attr("d", d3.arc()
                .innerRadius(0)
                .outerRadius(radius))
            // Can also be written as expression for evaluation,
            // no need to have curly braces and semicolon for
            // multi-line executable code statements. But then
            // is limited to only expressions though.
            .attr("fill", (d) => d.data[1].color)
            .on("click", (e, d) => { genderDistributionClick(d.data[0]); });
    }
}

let currentGenderSelection = -1;

function genderDistributionClick(gender)
{
    // TODO: So here we're setting global state for updateMapDeaths() to act
    // on. Not passing it as another parameter, not an object member. Refactor!
    currentGenderSelection = gender;

    {
        // TODO: Not nice. Recator eventually.

        let timeline = document.getElementById("timeline");

        if (timeline != null)
        {
            updateTimeline(timeline.parentNode.getAttribute("id"),
                           timelineWidth,
                           timelineHeight);
        }
    }

    updateMapDeaths();
}

function unsetTimelineFilter()
{
    currentDeathDaySelection = -1;

    for (let i = 0, max = currentDeathDayRange.length; i < max; i++)
    {
        if (currentDeathDayRange[i] != null)
        {
            currentDeathDayRange[i].element.parentNode.removeChild(currentDeathDayRange[i].element);
            currentDeathDayRange[i] = null;
        }
    }

    {
        // TODO: Not nice. Refactor eventually.

        let pie = document.getElementById("pie");

        if (pie != null)
        {
            updateGenderDistribution(pie.parentNode.getAttribute("id"),
                                     pieWidth,
                                     pieHeight);
        }
    }

    updateMapDeaths();
}

function unsetGenderDistributionFilter()
{
    currentGenderSelection = -1;

    {
        // TODO: Not nice. Refactor eventually.

        let timeline = document.getElementById("timeline");

        if (timeline != null)
        {
            updateTimeline(timeline.parentNode.getAttribute("id"),
                           timelineWidth,
                           timelineHeight);
        }
    }

    updateMapDeaths();
}

function unsetMapFilter()
{
    for (let i = 0, max = currentMapRange.length; i < max; i++)
    {
        for (let j = 0, max2 = currentMapRange[i].length; j < max2; j++)
        {
            if (currentMapRange[i][j] != null)
            {
                currentMapRange[i][j].element.parentNode.removeChild(currentMapRange[i][j].element);
                currentMapRange[i][j] = null;
            }
        }
    }

    updateMapDeaths();
}

function drawAgeGroups(ageGroupTargetId,
                       width,
                       height)
{
    d3.csv('./data/1850_census_us.csv')
    .then(function(ageRange)
    {
        let deathsPerAge = new Map();
        let deathCount = 0;

        for (const [deathDay, values] of deathsPerDay)
        {
            for (let i = 0, max = values.deaths.length; i < max; i++)
            {
                deathCount++;

                let age = values.deaths[i].age;

                if (deathsPerAge.has(age) != true)
                {
                    deathsPerAge.set(age, 1);
                }
                else
                {
                    deathsPerAge.set(age, deathsPerAge.get(age) + 1);
                }
            }
        }

        if (deathsPerAge.size != ageRange.length)
        {
            console.log("Census age group count and count of deaths per age group do not match!");
            return -1;
        }

        let percentMax = 0.0;
        let labels = new Array();

        let data = new Array();

        // Looping over array provides an ordered index/sequence,
        // vs. iterating over the "unordered" map (JavaScript Map
        // is ordered in the sequence of insertion, whichever
        // deaths of what age group came first).
        for (let i = 0, max = ageRange.length; i < max; i++)
        {
            let deathsPercent = (deathsPerAge.get(i) / deathCount) * 100.0;

            data.push({ source: 0,
                        // All styling could also be done with classes + CSS.
                        color: "#FFD966",
                        percent: deathsPercent });

            if (deathsPercent > percentMax)
            {
                percentMax = deathsPercent;
            }

            let censusPercent = parseFloat(ageRange[i].percent)

            data.push({ source: 1,
                        color: "#A8D08C",
                        percent: censusPercent });

            if (censusPercent > percentMax)
            {
                percentMax = censusPercent;
            }

            labels.push({ name: ageRange[i]["age-range"] });
        }

        let offsetY = 10;

        let scaleY = d3.scaleLinear()
           //.domain([0, d3.max(data, (d) => d.percent)])
           .domain([0, percentMax])
           .range([height - offsetY, 0]);

        let svgAgeGroups = d3.select("#" + ageGroupTargetId)
            .append("svg")
            .attr("id", "age-groups")
            .attr("width", width)
            .attr("height", height)
            .style("background-color", "white");

        let axisYPaddingX = 65;
        let padding = 1;
        let paddingGroup = 5;
        let ageGroupWidth = (width - (((data.length * padding) + ((data.length / 2) * paddingGroup)) + axisYPaddingX)) / data.length;
        let ageGroupHeightFactor = (height - offsetY) / percentMax;

        svgAgeGroups.selectAll("rect.age-group")
            .data(data)
            .enter()
            .append("rect")
            .attr("class", "age-group")
            .attr("x", (d, i) => axisYPaddingX + (i * ageGroupWidth) + (i * padding) + (((i - (i % 2)) / 2) * paddingGroup))
            //.attr("y", (d, i) => height - (d.percent * ageGroupHeightFactor))
            .attr("y", (d, i) => scaleY(d.percent) + (offsetY / 2))
            //.attr("cx", (d) => 100)
            .attr("cy", (d) => scaleY(d.percent))
            .attr("width", ageGroupWidth)
            .attr("height", (d, i) => (d.percent * ageGroupHeightFactor))
            .attr("fill", (d, i) => d.color);

        let axisY = d3.axisLeft()
            .scale(scaleY)
            .ticks(5);

        svgAgeGroups.append("g")
            .attr("transform", "translate(" + (axisYPaddingX - 10) + "," + (offsetY / 2) + ")")
            .call(axisY);

        svgAgeGroups.selectAll("text.age-group-label-percent")
            .data(data)
            .enter()
            .append("text")
            .attr("class", "age-group-label-count")
            .attr("fill", "#FFFFFF")
            // JavaScript does not help rounding on decimals?!
            .text((d) => "" + Math.round(d.percent) + "%")
            .attr("x", (d, i) => axisYPaddingX + (i * ageGroupWidth) + (i * padding) + (((i - (i % 2)) / 2) * paddingGroup) + (ageGroupWidth / 2))
            .attr("y", (d, i) => (height - (offsetY / 2)) - ((d.percent * ageGroupHeightFactor) / 2))
            .attr("text-anchor", "middle")
            .attr("dominant-baseline", "middle");

        // D3 does not support labels for axis! So we do without D3.
        let target = document.getElementById("age-groups");

        let labelAxisY = document.createElementNS("http://www.w3.org/2000/svg", "text");
        labelAxisY.setAttribute("fill", "#000000");
        labelAxisY.setAttribute("font-size", 12);
        labelAxisY.setAttribute("x", 15);
        labelAxisY.setAttribute("y", height / 2);
        labelAxisY.setAttribute("text-anchor", "middle");
        labelAxisY.setAttribute("dominant-baseline", "middle");
        labelAxisY.setAttribute("transform", "rotate(270," + 15 + "," + (height / 2) + ")");
        labelAxisY.appendChild(document.createTextNode("% Deaths vs. Census Population"));
        target.appendChild(labelAxisY);

        // TODO: Put these into a <g/>.

        let labelAxisXGroupDeathsRect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
        labelAxisXGroupDeathsRect.setAttribute("x", width * 0.6);
        labelAxisXGroupDeathsRect.setAttribute("y", 15);
        labelAxisXGroupDeathsRect.setAttribute("width", 20);
        labelAxisXGroupDeathsRect.setAttribute("height", 20);
        labelAxisXGroupDeathsRect.setAttribute("fill", "#FFD966");
        target.appendChild(labelAxisXGroupDeathsRect);

        let labelAxisXGroupDeathsText = document.createElementNS("http://www.w3.org/2000/svg", "text");
        labelAxisXGroupDeathsText.setAttribute("fill", "#000000");
        labelAxisXGroupDeathsText.setAttribute("font-size", 12);
        labelAxisXGroupDeathsText.setAttribute("x", (width * 0.6) + 20 + 5);
        labelAxisXGroupDeathsText.setAttribute("y", 15 + (0.75 * 20));
        labelAxisXGroupDeathsText.appendChild(document.createTextNode("1854 Deaths"));
        target.appendChild(labelAxisXGroupDeathsText);

        let labelAxisXGroupCensusRect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
        labelAxisXGroupCensusRect.setAttribute("x", width * 0.6);
        labelAxisXGroupCensusRect.setAttribute("y", 15 + 5 + 20);
        labelAxisXGroupCensusRect.setAttribute("width", 20);
        labelAxisXGroupCensusRect.setAttribute("height", 20);
        labelAxisXGroupCensusRect.setAttribute("fill", "#A8D08C");
        target.appendChild(labelAxisXGroupCensusRect);

        let labelAxisXGroupCensusText = document.createElementNS("http://www.w3.org/2000/svg", "text");
        labelAxisXGroupCensusText.setAttribute("fill", "#000000");
        labelAxisXGroupCensusText.setAttribute("font-size", 12);
        labelAxisXGroupCensusText.setAttribute("x", (width * 0.6) + 20 + 5);
        labelAxisXGroupCensusText.setAttribute("y", 15 + 20 + 5 + (0.75 * 20));
        labelAxisXGroupCensusText.appendChild(document.createTextNode("1850 US Census"));
        target.appendChild(labelAxisXGroupCensusText);
    });
}
